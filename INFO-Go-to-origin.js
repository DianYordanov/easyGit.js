var exec = require('child_process').exec, child;

const functions = require('./functions');

function visitOrigin(error, stdout, stderr) {
    if (functions.testEmpty(stderr) !== 'empty-yes') {
        console.log('stderr: ' + stderr);
    }
    if (error !== null) {
        console.log('exec error: ' + error);
    }
    var opsys = process.platform;
    if (opsys == "darwin") {
        opsys = "MacOS";
    } else if (opsys == "win32" || opsys == "win64") {
        opsys = "Windows";
        console.log('pure stdout value : ' + stdout);
        stdout = stdout.replaceAll('git@github.com:','https://github.com/');
        // if(!stdout.includes('git@')){

        var readableURL = functions.MakegitURLtoNormalURL(stdout);
        var readableURL2 = functions.MakegitURLtoNormalURL2(stdout);
        // console.log('readableURL value : ' + readableURL);
        // console.log('readableURL2 value : ' + readableURL2);


        // }
        // else{
            // var readableURL = 'functions.MakegitURLtoNormalURL(stdout)';
            // var readableURL2 = 'functions.MakegitURLtoNormalURL2(stdout)';
        // }

        if (readableURL === undefined){
            readableURL = stdout;
        }
        if (readableURL2 === undefined){
            readableURL2 = stdout;
        }

        readableURL = readableURL.replaceAll('.git.git', '.git');
        readableURL2 = readableURL2.replaceAll('.git.git', '.git');

        // console.log('stdout value : ' + stdout);
        // console.log('readableURL value : ' + readableURL);
        // console.log('readableURL2 value : ' + readableURL2);
        // console.log('readableURL: ' + functions.validURL(readableURL));
        // console.log('readableURL2: ' + functions.validURL(readableURL2));
        if(functions.validURL(readableURL2)){
            child = exec('start ' + readableURL2,
            { maxBuffer: 1024 * 1024 },
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                if (functions.testEmpty(stderr) !== 'empty-yes') {
                    console.log('stderr: ' + stderr);
                }
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        }
        else if(functions.validURL(readableURL)){
            child = exec('start ' + readableURL,
            { maxBuffer: 1024 * 1024 },
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                if (functions.testEmpty(stderr) !== 'empty-yes') {
                    console.log('stderr: ' + stderr);
                }
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        }
    } else if (opsys == "linux") {
        var readableURL = functions.MakegitURLtoNormalURL(stdout);
        child = exec('firefox ' + readableURL,
            { maxBuffer: 1024 * 1024 },
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                if (functions.testEmpty(stderr) !== 'empty-yes') {
                    console.log('stderr: ' + stderr);
                }
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
    }
    console.log(opsys) // I don't know what linux is.
}

module.exports = {
    GoToOrigin: (hashedDir) => {
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        // child = exec( dir + 'git config --get remote.origin.url',
        child = exec( dir + 'git remote -v',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            var stdoutArray = stdout.split(/\r?\n/);
            // console.log('stdout: ' + stdout);
            for(i=0;i<stdoutArray.length;i++){
                if(stdoutArray[i]!== "" && stdoutArray[i].split(/\s+/)[2]==="(push)"){
                    console.log('stdout: ' + stdoutArray[i].split(/\s+/)[1]);
                    visitOrigin(error, stdoutArray[i].split(/\s+/)[1], stderr);
                }
            }
        });
    }
};

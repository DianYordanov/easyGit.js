var exec = require('child_process').exec, child;

const functions = require('./functions');

module.exports = {
    GitShowAllRemotes: () => {
        functions.MakeTerminalCallFromMenuName('git remote -v');
    }
};